minetest.register_node("digistuff:detector", {
	tiles = {
	"digistuff_digidetector.png"
	},
	digiline = 
	{
		receptor = {},
		effector = {
			action = function (pos, node, channel, msg)
				local meta = minetest.get_meta (pos)
				if channel ~= meta:get_string ("channel") then
					return
				end
				local radius = tonumber (msg)
				if not radius then
					return
				end
				radius = math.min (radius, 10)
				radius = math.max (radius, 0)
				local players_found = {}
				local objs = minetest.get_objects_inside_radius(pos,radius)
				if objs then
					for i = 1, #objs, 1 do
						local obj = objs[i]
						if obj:is_player() then
							players_found[#players_found+1] = {
								name = obj:get_player_name(),
								pos = obj:get_pos(),
								pitch = obj:get_look_vertical(),
								yaw = obj:get_look_horizontal(),
							}
						end
					end
				end
				digiline:receptor_send(pos, digiline.rules.default, channel, players_found)
			end
		}
	},
	groups = {cracky=2},
	description = "Digilines Player Detector",
	on_construct = function(pos)
		local meta = minetest.get_meta(pos)
		meta:set_string("formspec","size[8,3;]field[1,1;6,2;channel;Channel;${channel}]button_exit[2.25,2;3,1;submit;Save]")
	end,
	_digistuff_channelcopier_fieldname = "channel",
	on_receive_fields = function(pos, formname, fields, sender)
		local name = sender:get_player_name()
		if minetest.is_protected(pos,name) and not minetest.check_player_privs(name,{protection_bypass=true}) then
			minetest.record_protection_violation(pos,name)
			return
		end
		local meta = minetest.get_meta(pos)
		if fields.channel then meta:set_string("channel",fields.channel) end
	end,
	sounds = default and default.node_sound_stone_defaults()
})

minetest.register_craft({
	output = "digistuff:detector",
	recipe = {
		{"mesecons_detector:object_detector_off"},
		{"mesecons_luacontroller:luacontroller0000"},
		{"digilines:wire_std_00000000"}
	}
})
