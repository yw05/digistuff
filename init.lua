digistuff = {}

local components = {
	"internal",
	"conductors",
	"touchscreen",
	"light",
	"noteblock",
	"switches",
	"panel",
	"piezo",
	"detector",
	"camera",
	"piston",
	"cardreader",
	"channelcopier",
	-- "controller",
}

if minetest.get_modpath("mesecons_luacontroller") then table.insert(components,"ioexpander") end

for i = 1, #components, 1 do
	dofile(string.format("%s%s%s.lua",minetest.get_modpath(minetest.get_current_modname()),DIR_DELIM,components[i]))
end